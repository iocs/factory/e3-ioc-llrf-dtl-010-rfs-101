require essioc

require sis8300llrf
require llrfsystem

epicsEnvSet("LLRF_P", "DTL-010")
epicsEnvSet("IDX", "101")
epicsEnvSet("LLRF_R", "RFS-LLRF-$(IDX)")
epicsEnvSet("LLRF_DIG_R_1", "RFS-DIG-$(IDX)")
epicsEnvSet("LLRF_RFM_R_1", "RFS-RFM-$(IDX)")
epicsEnvSet("TSELPV", "$(LLRF_P):RFS-EVR-$(IDX):EvtACnt-I.TIME")
epicsEnvSet("RKLY", "RFS-Kly-110:")
epicsEnvSet("RCAV", "EMR-CAV-010")
epicsEnvSet("SEC", "dtl")
epicsEnvSet("RFSTID", "4")

# Load standard module startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")

iocshLoad("$(llrfsystem_DIR)/llrfsystem.iocsh")

iocInit
